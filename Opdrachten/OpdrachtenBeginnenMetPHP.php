<?php

// 1. $color = array('white', 'green', 'red', 'blue', 'black');
// Write a script which will display the following string - Go to the editor
// "The memory of that scene for me is like a frame of film forever frozen at that moment: the red carpet, the green lawn, the white house, the leaden sky. The new president and his first lady. - Richard M. Nixon"
// and the words 'red', 'green' and 'white' will come from $color.

function arrays_1()
{
    $color = array('white', 'green', 'red', 'blue', 'black');
    echo "The memory of that scene for me is like a frame of film forever frozen at that moment: the $color[2] carpet, the $color[1] lawn, the $color[0] house, the leaden sky. The new president and his first lady. - Richard M. Nixon"."\n";
};

// 2. $color = array('white', 'green', 'red'') Go to the editor
// Write a PHP script which will display the colors in the following way :
// Output :
// white, green, red,

//     green
//     red
//     white

function arrays_2()
{
    $color = array('white', 'green', 'red');
    echo "$color[1],$color[2],$color[0]";
};

// 3. $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw") ;

// Create a PHP script which displays the capital and country name from the above array $ceu. Sort the list by the capital of the country.

function arrays_3()
{
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw") ;
    foreach ($ceu as $country => $city) {
        echo "the capital of $country is $city <br>";
    }
};

// 4. $x = array(1, 2, 3, 4, 5);
// Delete an element from the above PHP array. After deleting the element, integer keys must be normalized. Go to the editor
// Sample Output :
// array(5) { [0]=> int(1) [1]=> int(2) [2]=> int(3) [3]=> int(4) [4]=> int(5) }
// array(4) { [0]=> int(1) [1]=> int(2) [2]=> int(3) [3]=> int(5) }

function arrays_4()
{
    $x = array(1, 2, 3, 4, 5);
    var_dump($x);
    unset($x[3]);
    echo "<br>";
    var_dump($x);
    $x = array_values($x);
    echo "<br>";
    var_dump($x);
};

// 5. $color = array(4 => 'white', 6 => 'green', 11=> 'red');
// Write a PHP script to get the first element of the above array. Go to the editor
// Expected result : white

function arrays_5()
{
    $color = array(4 => 'white', 6 => 'green', 11=> 'red');
    echo reset($color) . "<br>";
    // echo next($color) . "<br>";
    // echo prev($color);
    // var_dump($color);
    // $color = array_values($color);
    // var_dump($color);
};

// 6. Write a PHP script which decodes the following JSON string. Go to the editor
// Sample JSON code :
// {"Title": "The Cuckoos Calling",
// "Author": "Robert Galbraith",
// "Detail": {
// "Publisher": "Little Brown"
// }}
// Expected Output :
// Title : The Cuckoos Calling
// Author : Robert Galbraith
// Publisher : Little Brown

function arrays_6()
{
    function walkthrough($value, $key)
    {
        echo "$key : $value <br>";
    }
    $string = '{"Title": "The Cuckoos Calling",
        "Author": "Robert Galbraith",
        "Detail": {
        "Publisher": "Little Brown"
         }}';
    
    $json1 = json_decode($string, true);
    var_dump($json1);
    echo "<br> <br>";
    array_walk_recursive($json1, "walkthrough");
};

// 7. Write a PHP script that inserts a new item in an array in any position. Go to the editor
// Expected Output :
// Original array :
// 1 2 3 4 5
// After inserting '$' the array is :
// 1 2 3 $ 4 5

function arrays_7()
{
    $numbers = array(1,2,3,4,5);
    array_splice($numbers, 3, 0, '$');
    foreach ($numbers as $x) {
        echo "$x ";
    }
};


// 8. Write a PHP script to sort the following associative array : Go to the editor
// array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40") in
// a) ascending order sort by value
// b) ascending order sort by Key
// c) descending order sorting by Value
// d) descending order sorting by Key

function arrays_8()
{
    $test = array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
    echo "a) ascending order sort by value <br>";
    asort($test);
    foreach ($test as $key => $value) {
        echo "waarde: $key : $value <br>";
    };

    echo "<br>b) Ascending order sort by key <br>";
    ksort($test);
    foreach ($test as $key => $value) {
        echo "waarde: $key : $value <br>";
    };

    echo "<br>b) Descending order sort by value <br>";
    arsort($test);
    foreach ($test as $key => $value) {
        echo "waarde: $key : $value <br>";
    };

    echo "<br>b) Descending order sort by key <br>";
    krsort($test);
    foreach ($test as $key => $value) {
        echo "waarde: $key : $value <br>";
    };
};

// 9. Write a PHP script to calculate and display average temperature, five lowest and highest temperatures. Go to the editor
// Recorded temperatures : 78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73
// Expected Output :
// Average Temperature is : 70.6
// List of seven lowest temperatures : 60, 62, 63, 63, 64,
// List of seven highest temperatures : 76, 78, 79, 81, 85

function arrays_9()
{
    $temps = '78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 81, 76, 73,
    68, 72, 73, 75, 65, 74, 63, 67, 65, 64, 68, 73, 75, 79, 73';
    $temp_array = explode(',', $temps);
    $totalTemps = 0;
    $temp_array_length = count($temp_array);
    foreach ($temp_array as $value) {
        $totalTemps += $value;
    }
    $avg_temps = $totalTemps / $temp_array_length;
    echo "Average temperature is $avg_temps <br>";
    sort($temp_array);
    echo "The five lowest temperatures: ";
    for ($i=0; $i < 5; $i++) {
        echo "$temp_array[$i] , ";
    }
    echo "<br>The five highest temperatures: ";
    for ($i=$temp_array_length-5; $i < $temp_array_length; $i++) {
        echo "$temp_array[$i] , ";
    }
};

// 10. Write a PHP program to sort an array of positive integers using the Bead-Sort Algorithm. Go to the editor
// According to Wikipedia "Bead-sort is a natural sorting algorithm, developed by Joshua J. Arulanandham, Cristian S. Calude and Michael J.
// Dinneen in 2002. Both digital and analog hardware implementations of bead sort can achieve a sorting time of O(n);
// however, the implementation of this algorithm tends to be significantly slower in software and can only be used to sort lists of positive integers".

function arrays_10()
{
    function columns($uarr)
    {
        $n=$uarr;
        if (count($n) == 0) {
            return array();
        } elseif (count($n) == 1) {
            return array_chunk($n[0], 1);
        }
        array_unshift($uarr, null);
        $transpose = call_user_func_array('array_map', $uarr);
        return array_map('array_filter', $transpose);
    }
    function bead_sort($uarr)
    {
        foreach ($uarr as $e) {
            $poles []= array_fill(0, $e, 1);
        }
        return array_map('count', columns(columns($poles)));
    }
    echo 'Original Array : '.'
';
    print_r(array(5,3,1,3,8,7,4,1,1,3));
    echo '
'.'After Bead sort : '.'
';
    print_r(bead_sort(array(5,3,1,3,8,7,4,1,1,3)));
};

// 11. Write a PHP program to merge (by index) the following two arrays. Go to the editor
// Sample arrays :
// $array1 = array(array(77, 87), array(23, 45));
// $array2 = array("w3resource", "com");
// Expected Output :

// Array
// (
// [0] => Array
// (
// [0] => w3resource
// [1] => 77
// [2] => 87
// )
// [1] => Array
// (
// [0] => com
// [1] => 23
// [2] => 45
// )
// )

function arrays_11()
{
    $array1 = array(array(77, 87), array(23, 45));
    $array2 = array("w3resource", "com");
    function merge_arrays_by_index($x, $y)
    {
        $temp = array();
        $temp[] = $x;
        if (is_scalar($y)) {
            $temp[] = $y;
        } else {
            foreach ($y as $k => $v) {
                $temp[] = $v;
            }
        }
        return $temp;
    }
    echo '<pre>';
    print_r(array_map('merge_arrays_by_index', $array2, $array1));
};

// 12. Write a PHP function to change the following array's all values to upper or lower case.
// Sample arrays :
// $Color = array('A' => 'Blue', 'B' => 'Green', 'c' => 'Red');
// Expected Output :
// Values are in lower case.
// Array ( [A] => blue [B] => green [c] => red )
// Values are in upper case.
// Array ( [A] => BLUE [B] => GREEN [c] => RED )
function arrays_12()
{
    function array_change_value_case($input, $ucase)
    {
        $case = $ucase;
        $narray = array();
        if (!is_array($input)) {
            return $narray;
        }
        foreach ($input as $key => $value) {
            if (is_array($value)) {
                $narray[$key] = array_change_value_case($value, $case);
                continue;
            }
            $narray[$key] = ($case == CASE_UPPER ? strtoupper($value) : strtolower($value));
        }
        return $narray;
    }
    $Color = array('A' => 'Blue', 'B' => 'Green', 'c' => 'Red');
    echo 'Actual array ';
    print_r($Color);
    echo 'Values are in lower case.';
    $myColor = array_change_value_case($Color, CASE_LOWER);
    print_r($myColor);
    echo 'Values are in upper case.';
    $myColor = array_change_value_case($Color, CASE_UPPER);
    print_r($myColor);
};
function arrays_13()
{
    echo implode(",", range(200, 250, 4))."\n";
};
function arrays_14()
{
    $my_array = array("abcd","abc","de","hjjj","g","wer");
    $new_array = array_map('strlen', $my_array);
    // Show maximum and minimum string length using max() function and min() function
    echo "The shortest array length is " . min($new_array) .
". The longest array length is " . max($new_array).'.';
};
function arrays_15()
{
    $n=range(11, 20);
    shuffle($n);
    for ($x=0; $x< 10; $x++) {
        echo $n[$x].' ';
    }
};
function arrays_16()
{
    $ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels",
"Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava",
"Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin",
"Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm",
"United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=> "Valetta","Austria" => "Vienna", "Poland"=>"Warsaw") ;
    $max_key = max(array_keys($ceu));
    echo $max_key."\n";
};
function arrays_17()
{
    function min_values_not_zero(array $values)
    {
        return min(array_diff(array_map('intval', $values), array(0)));
    }
    print_r(min_values_not_zero(array(-1,0,1,12,-100,1))."\n");
};
function arrays_18()
{
    function floorDec($number, $precision, $separator)
    {
        $number_part=explode($separator, $number);
        $number_part[1]=substr_replace($number_part[1], $separator, $precision, 0);
        if ($number_part[0]>=0) {
            $number_part[1]=floor($number_part[1]);
        } else {
            $number_part[1]=ceil($number_part[1]);
        }

        $ceil_number= array($number_part[0],$number_part[1]);
        return implode($separator, $ceil_number);
    }
    print_r(floorDec(1.155, 2, ".")."\n");
    print_r(floorDec(100.25781, 4, ".")."\n");
    print_r(floorDec(-2.9636, 3, ".")."\n");
};
function arrays_19()
{
    $color = array( "color" => array( "a" => "Red", "b" => "Green", "c" => "White"),
"numbers" => array( 1, 2, 3, 4, 5, 6 ),
"holes" => array( "First", 5 => "Second", "Third"));
    echo $color["holes"][5]."\n"; // prints "second"
echo $color["color"]["a"]."\n"; // prints "Red"
};

// oefeningen Functions

// 1. Write a function to calculate the factorial of a number (a non-negative integer). The function accepts the number as an argument.
function functions_1()
{
    function factorial($number)
    {
        $total = 1;
        for ($i=$number; $i > 0 ; $i--) {
            $total *= $i;
        }
        echo "The factorial of $number = $total";
    }
    factorial(5);
    

    // S

//     function factorial_of_a_number($n)
// {
//   if($n ==0)
//     {
// 	   return 1;
//     }
//   else
//     {
// 	   return $n * factorial_of_a_number($n-1);
//     }
// 	}
// print_r(factorial_of_a_number(4)."\n");
}

// 2. Write a function to check a number is prime or not.
function functions_2()
{
    function isPrime($n)
    {
        for ($x=2; $x<$n; $x++) {
            if ($n %$x ==0) {
                return 0;
            }
        }
        return 1;
    }
    $a = IsPrime(5);
    if ($a==0) {
        echo 'not a Prime Number';
    } else {
        echo 'Prime Number';
    }
}

// 3. Write a function to reverse a string.
function functions_3()
{
    function reverse_string($string)
    {
        $n = strlen($string);
        if ($n == 1) {
            return $string;
        } else {
            $n--;
            return reverse_string(substr($string, 1, $n)) . substr($string, 0, 1);
        }
    }
    echo(reverse_string('1234'));
}

// 4. Write a function to sort an array.
function functions_4()
{
    function array_sort($a)
    {
        for ($x=0;$x< count($a);++$x) {
            $min = $x;
            for ($y=$x+1;$y< count($a);++$y) {
                if ($a[$y] < $a[$min]) {
                    $temp = $a[$min];
                    $a[$min] = $a[$y];
                    $a[$y] = $temp;
                }
            }
        }
        return $a;
    }
    $a = array(51,14,1,21,'hj');
    print_r(array_sort($a));
}

// 5. Write a PHP function that checks if a string is all lowercase.
function functions_5()
{
    function is_str_lowercase($str1)
    {
        for ($sc = 0; $sc < strlen($str1); $sc++) {
            if (ord($str1[$sc]) >= ord('A') &&
          ord($str1[$sc]) <= ord('Z')) {
                return false;
            }
        }
        return true;
    }
    var_dump(is_str_lowercase('abc def ghi'));
    var_dump(is_str_lowercase('abc dEf ghi'));
}

// 6. Write a PHP function that checks whether a passed string is a palindrome or not?
function functions_6()
{
    function check_palindrome($string)
    {
        if ($string == strrev($string)) {
            return 1;
        } else {
            return 0;
        }
    }
    echo check_palindrome('madam');
}
function loops_1()
{
    for ($x=1; $x<=10; $x++) {
        if ($x< 10) {
            echo "$x-";
        } else {
            echo "$x"."\n";
        }
    }
}
function loops_2()
{
    $sum = 0;
    for ($x=1; $x<=30; $x++) {
        $sum +=$x;
    }
    echo "The sum of the numbers 0 to 30 is $sum"."\n";
}
function loops_3()
{
    for ($x=1;$x<=5;$x++) {
        for ($y=1;$y<=$x;$y++) {
            echo "*";
            if ($y< $x) {
                echo " ";
            }
        }
        echo "\n";
    }
}
function loops_4()
{
    $n=5;
    for ($i=1; $i<=$n; $i++) {
        for ($j=1; $j<=$i; $j++) {
            echo ' * ';
        }
        echo '<br>';
    }
    for ($i=$n; $i>=1; $i--) {
        for ($j=1; $j<=$i; $j++) {
            echo ' * ';
        }
        echo '<br>';
    }
}
function loops_5()
{
    $n = 6;
    $x = 1;
    for ($i=1;$i<=$n-1;$i++) {
        $x*=($i+1);
    }
    echo "The factorial of  $n = $x"."\n";
}
function loops_6()
{
    for ($a=0; $a< 10; $a++) {
        for ($b=0; $b< 10; $b++) {
            echo $a.$b.", ";
        }
    }
    printf("\n");
}
function loops_7()
{
    $text="w3resource";
    $search_char="r";
    $count="0";
    for ($x="0"; $x< strlen($text); $x++) {
        if (substr($text, $x, 1)==$search_char) {
            $count=$count+1;
        }
    }
    echo $count."\n";
}
function loops_8()
{
    echo "<table align=\"left\" border=\"1\" cellpadding=\"3\"cellspacing=\"0\" >";
    for ($i=1;$i<=6;$i++) {
        echo "<tr>";
        for ($j=1;$j<=5;$j++) {
            echo "<td>$i * $j = ".$i*$j."</td>";
        }
        echo "</tr>";
    }
    echo "</table>" . "<br><br><br><br><br><br><br><br><br>";
}
function loops_9()
{
    echo "<table width=\"270px\" cellspacing=\"0px\" cellpadding=\"0px\" border=\"1px\">";
    for ($row=1;$row<=8;$row++) {
        echo "<tr>";
        for ($col=1;$col<=8;$col++) {
            $total=$row+$col;
            if ($total%2==0) {
                echo "<td height=30px width=30px bgcolor=#FFFFFF></td>";
            } else {
                echo "<td height=30px width=30px bgcolor=#000000></td>";
            }
        }
        echo "</tr>";
    }
    echo "</table>";
}
function loops_10()
{
    echo "<table border =\"1\" style='border-collapse: collapse'>";
    for ($row=1; $row <= 10; $row++) {
        echo "<tr> \n";
        for ($col=1; $col <= 10; $col++) {
            $p = $col * $row;
            echo "<td>$p</td> \n";
        }
        echo "</tr>";
    }
    echo "</table>";
}

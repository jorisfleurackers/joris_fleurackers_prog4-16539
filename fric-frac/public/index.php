<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fric-Frac</title>
</head>
<body>
<header>
    <h1>Fric-Frac</h1>
</header>
<ul>
    <li><a href="./Country/index.php">index</a> - Land Lijst Weergeven</li>
    <li><a href="./Country/create.php">create</a> - Land Toevoegen</li>
    <li><a href="./Country/read.php">read</a> - Land Zoeken</li>
    <li><a href="./Country/update.php">update</a> - Land Wijzigen</li>
    <li><a href="./Country/delete.php">delete</a> - Land Verwijderen</li>
    <br>
    <br>
    <li><a href="./Role/index.php">index</a> - Rol Lijst Weergeven</li>
    <li><a href="./Role/create.php">create</a> - Rol Toevoegen</li>
    <li><a href="./Role/read.php">read</a> - Rol Zoeken</li>
    <li><a href="./Role/update.php">update</a> - Rol Wijzigen</li>
  <li><a href="./Role/delete.php">delete</a> - Rol Verwijderen</li>
  <br>
  <br>
  <li><a href="./EventCategory/index.php">index</a> - Event Categorie Lijst Weergeven</li>
  <li><a href="./EventCategory/create.php">create</a> - Event Categorie Toevoegen</li>
  <li><a href="./EventCategory/read.php">read</a> - Event Categorie Zoeken</li>
  <li><a href="./EventCategory/update.php">update</a> - Event Categorie Wijzigen</li>
  <li><a href="./EventCategory/delete.php">delete</a> - Event Categorie Verwijderen</li>
  <br>
  <br>
  <li><a href="./EventTopic/index.php">index</a> - Event Onderwerp Lijst Weergeven</li>
  <li><a href="./EventTopic/create.php">create</a> - Event Onderwerp Toevoegen</li>
  <li><a href="./EventTopic/read.php">read</a> - Event Onderwerp Zoeken</li>
  <li><a href="./EventTopic/update.php">update</a> - Event Onderwerp Wijzigen</li>
  <li><a href="./EventTopic/delete.php">delete</a> - Event Onderwerp Verwijderen</li>
</ul>
<?php include "templates/footer.php"; ?>
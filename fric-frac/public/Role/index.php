<?php
    try {
        include "../../config.php";
        include "../../common.php";
        $connection = new \PDO($host, $username, $password, $options);
        $sql = "SELECT * FROM Role";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
 include "../templates/header.php"; ?>
		<h2>Lijst van Rollen</h2>
        <a href="create.php">Rol Toevoegen</a>
        <br>
        <a href="read.php">Rollen Zoeken</a>
        <br>
		<table>
			<thead>
				<tr>									
					<th>Naam</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) {
            ?>
			<tr>
				<td><?php echo escape($row["Name"]); ?></td>	
                <td><a href="update-single.php?Id=<?php echo escape($row["Id"]); ?>">Edit</a></td>	
                <td><a href="delete.php?Id=<?php echo escape($row["Id"]); ?>">Delete</a></td>	
			</tr>
		<?php
        } ?> 
			</tbody>
	</table>
<?php include "../templates/footer.php"; ?>
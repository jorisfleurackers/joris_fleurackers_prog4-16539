<?php
try {
    include "../../config.php";
    include "../../common.php";
    $connection = new \PDO($host, $username, $password, $options);
    $sql = "SELECT * FROM EventTopic";
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll();
} catch (\PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
include "../templates/header.php"; ?>      
<h2>Update Event Topic</h2>
<table>
  <thead>
    <tr>
      <th>Name</th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
      <tr>
        <td><?php echo escape($row["Name"]); ?></td>
        <td><a href="update-single.php?Id=<?php echo escape($row["Id"]); ?>">Edit</a></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
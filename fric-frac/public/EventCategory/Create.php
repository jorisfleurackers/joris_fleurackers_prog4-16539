<?php
if (isset($_POST["submit"])) {
    include "../../config.php";
    include "../../common.php";
    $name = validateData($_POST["Name"]);
    if (empty($name)) {
        $nameErr = "Naam is een verplicht veld";
    } elseif (strlen($name) > 50) {
        $nameErr = "Naam is maximum 50 karakters lang";
    } else {
        try {
            $connection = new \PDO($host, $username, $password, $options);
            $new_eventCategory = array("Name" => $name);
            $sql = sprintf(
                "INSERT INTO %s (%s) values (%s)",
                "EventCategory",
                implode(", ", array_keys($new_eventCategory)),
                 ":" . implode(", :", array_keys($new_eventCategory))
                );
            $statement = $connection->prepare($sql);
            $statement->execute($new_eventCategory);
            header('Location: index.php');
        } catch (\PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}
 include "../templates/header.php";?>
<h2>Event Categorie Toevoegen</h2>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
<div>
    <label for="Name">Naam</label>
    <input type="text" name="Name" id="Name" required="required" value="<?php echo $name;?>">
    <span style="color:red"><?php echo " $nameErr "?></span>
</div>
<br>
<button type="submit" name="submit">Toevoegen</button>
</form>  
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
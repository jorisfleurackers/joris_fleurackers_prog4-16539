<?php
if (isset($_POST['submit'])) {
    if (strlen($_POST['Name']) > 50) {
        $nameErr = "Naam is maximum 50 karakters lang";
    } else {
        try {
            include "../../config.php";
            include "../../common.php";
            $connection = new \PDO($host, $username, $password, $options);       
            $sql = "SELECT * FROM Country WHERE Name = :Name";
            $statement = $connection->prepare($sql);
            $statement->bindParam(':Name', $_POST['Name'], PDO::PARAM_STR);
            $statement->execute();
            $result = $statement->fetchAll();
            $form = "submit";
        } catch (\PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}

if (isset($_POST['submitCode'])) {
    if (strlen($_POST['Code']) > 2) {
        $codeErr = "Code is maximum 2 karakters lang";
    } else {
        try {
            include "../../config.php";
            include "../../common.php";
            $connection = new \PDO($host, $username, $password, $options);
            $sql = "SELECT * FROM Country WHERE Code = :Code";
            $statement = $connection->prepare($sql);
            $statement->bindParam(':Code', $_POST['Code'], PDO::PARAM_STR);
            $statement->execute();
            $result = $statement->fetchAll();
            $form = "submitCode";
        } catch (PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}
    include "../templates/header.php"; 
if (isset($_POST[$form])) {
    if ($result && $statement->rowCount() > 0) {
        ?>
		<h2>Resultaten</h2>
		<table>
			<thead>
				<tr>									
					<th>Naam</th>
                    <th>Code</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) {
            ?>
			<tr>
				<td><?php echo escape($row["Name"]); ?></td>
                <td><?php echo escape($row["Code"]); ?></td>			
			</tr>
		<?php
        } ?> 
			</tbody>
	</table>
	<?php
    } else {
        ?>
		<blockquote>Geen resultaten gevonden.</blockquote>
	<?php
    }
} ?> 

<h2>Zoek land op Naam</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	<label for="Name">Naam</label>
	<input type="text" id="Name" name="Name" value="<?php echo($_POST['Name']);?>">
	<input type="submit" name="submit" value="Zoek">
    <span style="color:red"><?php echo " $nameErr "?></span>
</form>

<h2>Zoek land op Code</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	<label for="Code">Code</label>
	<input type="text" id="Code" name="Code" value="<?php echo($_POST['Code']);?>">
	<input type="submit" name="submitCode" value="Zoek">
	<span style="color:red"><?php echo " $codeErr "?></span>
</form>
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
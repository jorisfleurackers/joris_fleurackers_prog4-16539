<?php
if (isset($_POST["submit"])) {
    include "../../config.php";
    include "../../common.php";
    $name = validateData($_POST["Name"]);
    $code = validateData($_POST["Code"]);
    if (strlen($code) > 2) {
        $codeErr = "Code is maximum 2 karakters lang";
    } elseif (empty($name)) {
        $nameErr = "Naam is een verplicht veld";
    } elseif (strlen($name) > 50) {
        $nameErr = "Naam is maximum 50 karakters lang";
    } else {
        try {
            $connection = new \PDO($host, $username, $password, $options);
            $new_country = array("Name" => $name, "Code" => $code);
            $sql = sprintf(
                "INSERT INTO %s (%s) values (%s)",
                "Country",
                implode(", ", array_keys($new_country)),
                 ":" . implode(", :", array_keys($new_country))
                );
            $statement = $connection->prepare($sql);
            $statement->execute($new_country);
            header('Location: index.php');
        } catch (\PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}
 include "../templates/header.php";
?>
<h2>Land Toevoegen</h2>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
<div>
    <label for="Name">Naam</label>
    <input type="text" name="Name" id="Name" required="required" value="<?php echo $name;?>">
    <span style="color:red"><?php echo " $nameErr "?></span>
</div>
<div>
    <label for="Code">Code</label>
    <input type="text" name="Code" id="Code" value="<?php echo $code;?>">
    <span style="color:red"><?php echo " $codeErr "?></span>
</div>
<br>
<button type="submit" name="submit">Toevoegen</button>
</form>  
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
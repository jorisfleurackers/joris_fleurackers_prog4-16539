<?php
namespace Modernways\FricFrac\Dal;

class Role extends Base
{
    public static function delete($id)
    {
        $success = 0;
        if (self::connect()) {
            $Id = \ModernWays\Helpers::escape($id);
      
            try {
                $sql = 'DELETE FROM Role WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $Id);
                $statement->execute();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "De rij met id $id bestaat niet!";
                } else {
                    self::$message = "De rij met id $id is gedeleted!";
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Fout: verwijderen mislukt!";
            }
        }
        return $success;
    }
    public static function create($post)
    {
        $success = false;
        if (self::connect()) {
            $newRole = array(
                'Name' => \ModernWays\Helpers::escape($post['Name']),
            );
            try {
                $sql = sprintf(
                    "INSERT INTO %s (%s) VALUES (:%s)",
                    'Role',
                    implode(', ', array_keys($newRole)),
                    implode(', :', array_keys($newRole))
                );
                $statement = self::$connection->prepare($sql);
                $statement->execute($newRole);
                self::$message = 'Rij is toegevoegd!';
                $success = true;
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Rij is niet toegevoegd!";
            }
        }
        return $success;
    }
    
    public static function readOneByName($name)
    {
        $success = 0;
        if (self::connect()) {
            $Name = \ModernWays\Helpers::escape($name);
            try {
                $sql = 'SELECT * FROM Role WHERE Name = :Name';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Name', $name, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetchAll();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "Geen rol met de naam $name gevonden.";
                } else {
                    self::$message = "Alle rollen met de naam $name is gevonden.";
                }
            } catch (\PDOException $exception) {
                self::$message = "Syntax fout in SQL: {$exception->getMessage()}";
            }
        }
        return $success;
    }
    
    public static function readOneById($id)
    {
        $success = 0;
        if (self::connect()) {
            $Name = \ModernWays\Helpers::escape($id);
            try {
                $sql = 'SELECT * FROM Role WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $id, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetchAll();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "De rij met de id $id is niet ingelezen.";
                } else {
                    self::$message = "De rij met de id $id is ingelezen.";
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Geen rol met de id $id gevonden.";
            }
        }
        return $success;
    }
    public static function readAll()
    {
        $result = null;
        if (self::connect()) {
            try {
                $sql = 'SELECT * FROM Role';
                $statement = self::$connection->prepare($sql);
                $statement->execute();
                $result = $statement->fetchAll();
                self::$message = "Alle rijen van Role zijn ingelezen.";
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "De tabel Role is leeg.";
            }
        }
        return $result;
    }
    public static function Update($post)
    {
        $success = 0;
        if (self::connect()) {
            $updateRole= array(
                'Id'=> \ModernWays\Helpers::escape($post['Id']),
                'Name'=> \ModernWays\Helpers::escape($post['Name']),
            );

            try {
                $sql= 'UPDATE Role SET Name = :Name
                    WHERE Id = :Id';
                $statement= self::$connection->prepare($sql);
                $statement -> bindParam(':Id', $updateRole['Id']);
                $statement -> bindParam(':Name', $updateRole['Name']);
                $statement->execute($updateRole);
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "De rol met de naam {$updateRole['Name']} is niet gevonden.";
                } else {
                    self::$message = "De rol met de naam {$updateRole['Name']} is geüpdated.";
                }
            } catch (\PDOException $exception) {
                self::$message = "De rol met de naam {$updateRole['Name']} is niet geüpdated. Syntax error: { $exception->getMessage()}";
            }
        }
        return $success;
    }
}

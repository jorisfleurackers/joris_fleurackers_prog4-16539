<?php
if (isset($_POST['submit'])) {
    include "../../config.php";
    include "../../common.php";
    $myid = $_POST['Id'];
    $name = validateData($_POST["Name"]);
    $code = validateData($_POST["Code"]);
    if (strlen($code) > 2) {
        $codeErr = "Code is maximum 2 karakters lang";
    } elseif (empty($name)) {
        $nameErr = "Naam is een verplicht veld";
    } elseif (strlen($name) > 50) {
        $nameErr = "Naam is maximum 50 karakters lang";
    } else {
        try {
            $connection = new \PDO($host, $username, $password, $options);
            $sql = "UPDATE Country SET  Name = :Name, Code = :Code WHERE Id = :Id";
            $statement = $connection->prepare($sql);
            $statement->bindValue(':Id', $myid);
            $statement->bindValue(':Name', $name);
            $statement->bindValue(':Code', $code);
            $statement->execute();
            header('Location: index.php');
        } catch (\PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}
if (isset($_GET['Id'])) {
    include "../../config.php";
    include "../../common.php";
    try {
        $connection = new \PDO($host, $username, $password, $options);
        $sql = "SELECT * FROM Country WHERE Id = :Id";
        $statement = $connection->prepare($sql);
        $statement->bindValue(':Id', $_GET['Id']);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        $myid = $result["Id"];
        $name = $result["Name"];
        $code = $result["Code"];
    } catch (\PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
 include "../templates/header.php";
 ?>

<h2>Wijzig Land</h2>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
<div>
    <input type="hidden" name="Id" id="Id" required="required" value="<?php echo escape($myid);?>">
</div>
<div>
    <label for="Name">Naam</label>
    <input type="text" name="Name" id="Name" required="required" value="<?php echo escape($name); ?>">
    <span style="color:red"><?php echo " $nameErr "?></span>
</div>
<div>
    <label for="Code">Code</label>
    <input type="text" name="Code" id="Code" value="<?php echo escape($code); ?>">
    <span style="color:red"><?php echo " $codeErr "?></span>
</div>
<br>
<button type="submit" name="submit">Wijzig</button>
</form> 
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
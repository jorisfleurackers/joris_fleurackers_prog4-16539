<?php
include_once ('../.../Dal/Base.php');
include_once ('../../Dal/Country.php');
 include "../templates/header.php";
?>
<h2>Land Toevoegen</h2>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
<div>
    <label for="Name">Naam</label>
    <input type="text" name="Name" id="Name" required="required" value="<?php echo $name;?>">
    <span style="color:red"><?php echo " $nameErr "?></span>
</div>
<div>
    <label for="Code">Code</label>
    <input type="text" name="Code" id="Code" value="<?php echo $code;?>">
    <span style="color:red"><?php echo " $codeErr "?></span>
</div>
<br>
<button type="submit" name="submit">Toevoegen</button>
</form>  
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
<?php
if (isset($_POST['submit'])) {
	if (strlen($_POST['Name']) > 50) {
        $nameErr = "Naam is maximum 50 karakters lang";
    } else {
		try {
			include "../../config.php";
			include "../../common.php";
	
			$connection = new \PDO($host, $username, $password, $options);
			$sql = "SELECT * FROM EventCategory WHERE Name = :Name";
			$Name = $_POST['Name'];
			$statement = $connection->prepare($sql);
			$statement->bindParam(':Name', $Name, PDO::PARAM_STR);
			$statement->execute();
			$result = $statement->fetchAll();
		} catch (PDOException $error) {
			echo $sql . "<br>" . $error->getMessage();
		}
	}
}
include "../templates/header.php"; 
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) {
        ?>
		<h2>Resultaten</h2>
		<table>
			<thead>
				<tr>					
					<th>Naam</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) {
            ?>
			<tr>
				<td><?php echo escape($row["Name"]); ?></td>			
			</tr>
		<?php
        } ?> 
			</tbody>
	</table>
	<?php
    } else {
        ?>
		<blockquote>Geen resultaten gevonden voor <?php echo escape($_POST['Name']); ?>.</blockquote>
	<?php
    }
} ?> 
<h2>Zoek op naam</h2>
<form method="post">
	<label for="Name">naam</label>
	<input type="text" id="Name" name="Name">
	<input type="submit" name="submit" value="View Results">
</form>
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
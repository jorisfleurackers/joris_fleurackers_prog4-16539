<?php
if (isset($_GET["Id"])) {
    try {
        include "../../config.php";
        include "../../common.php";
        $connection = new \PDO($host, $username, $password, $options);
        $sql = "DELETE FROM EventTopic WHERE Id = :Id";
        $statement = $connection->prepare($sql);
        $statement->bindValue(':Id', $_GET["Id"]);
        $statement->execute();
        header('Location: index.php');
    } catch (\PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
try {
    include "../../config.php";
    include "../../common.php";
    $connection = new \PDO($host, $username, $password, $options);
    $sql = "SELECT * FROM EventTopic";
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll();
} catch (\PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>
<?php include "../templates/header.php"; ?>      
<h2>Verwijder Event Topic</h2>
<table>
  <thead>
    <tr>
      <th>Naam</th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
      <tr>
        <td><?php echo escape($row["Name"]); ?></td>
        <td><a href="delete.php?Id=<?php echo escape($row["Id"]); ?>">Delete</a></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<a href="index.php">naar lijst</a>
<?php include "../templates/footer.php"; ?>
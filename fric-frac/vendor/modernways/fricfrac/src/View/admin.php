<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <title>Beheer Fric-frac</title>
</head>
<body>
    <main>
        <header><h1>Fric-frac</h1></header>
        <section>
            <a href="Person/Index.php" class="tile"><span></span></a>
            <a href="Country/Index.php"class="tile"><span>Land</span></a>
            <a class="tile"></a>
            <a class="tile"></a>
            <a href="Role/Index.php"class="tile"><span>Rol</span></a>
            <a href="User/Index.php" class="tile"><span></span></a>
            <a class="tile"></a>
            <a href="Event/Index.php" class="tile"><span></span></a>
            <a href="EventCategory/Index.php" class="tile"><span>Event Categorie</span></a>
            <a href="EventTopic/Index.php"class="tile"><span>Event Topic</span></a>
            <a class="tile"></a>
        </section>
        <footer><p>Fric-frac</p></footer>
    </main>
</body>
</html>
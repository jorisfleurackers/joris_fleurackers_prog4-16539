<?php
include('../vendor/autoload.php');

$request = $_SERVER['REQUEST_URI'];
$selfme = $_SERVER['PHP_SELF'];

switch ($request) {
    case '/parkenApp-php-api/public/curiosity/readall':
        \Jobin\Dal::$configLocation = __DIR__ . '/../data/config.ini';
        $list = \Jobin\Dal::readAll('Curiosity');
        $json = json_encode($list);
        echo $json;
        break;
    case '/parkenApp-php-api/public/curiositycomment/create':
        $newComment = $_POST;
        \Jobin\Dal::$configLocation = __DIR__ . '/../data/config.ini';
        if (\Jobin\Dal::create('CuriosityComment', $newComment, 'Comment')) {
            echo 'Create is gelukt!<br/>';
        } else {
            echo 'Oeps er is iets fout gelopen!</br>';
        }
        echo \Jobin\Dal::getMessage();
        break;
     case '/parkenApp-php-api/public/curiositycomment/createcheck':
        if (isset($_POST['UserName']) && isset($_POST['Password'])) {
            \Jobin\Dal::$configLocation = __DIR__ . '/../data/config.ini';
            // find user and verify password
            $user = \Jobin\Dal::readOne('User', $_POST['UserName'], 'Name', array('Name', 'HashedPassword', 'RoleId', 'Salt'));
            if (isset($user)) {
                if (password_verify($_POST['Password'] . $user['Salt'], $user['HashedPassword'])) {
                    $newComment = array('UserName' => $user['Name'], 'Comment' => $_POST['Comment'], 'CuriosityId' => $_POST['CuriosityId']);
                    if (\Jobin\Dal::create('CuriosityComment', $newComment, 'Comment')) {
                        echo 'Create is gelukt!<br/>';
                    } else {
                        echo 'Oeps er is iets fout gelopen!</br>';
                    }
                    echo \Jobin\Dal::getMessage();
                } else {
                    echo 'Ongeldig paswoord!';
                }
            } else {
                echo 'Ongeldige gebruikernaam!';
            }
        } else {
            echo 'Je moet een gebruikernaam en paswoord opgeven!';
        }
            
        break;
    default:
    if ($selfme = '/parkenApp-php-api/public/curiositycomment/readallwhere') {
        if (isset($_GET['CuriosityId'])) {
            \Jobin\Dal::$configLocation = __DIR__ . '/../data/config.ini';
            $list = \Jobin\Dal::readAllWhere('CuriosityComment', $_GET['CuriosityId'], 'CuriosityId', 'UserName');
            // $json = '{' . json_encode($list) . '}';
            echo json_encode($list);
        } else {
            echo "$request is invalid and cannot be processed.";
        }
    } else {
        echo "$request not found.";
    }
        
        break;
}
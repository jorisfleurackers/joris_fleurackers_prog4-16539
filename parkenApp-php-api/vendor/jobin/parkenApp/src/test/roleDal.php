<?php
include ('../../../../autoload.php');

\Jobin\Dal::$configLocation = __DIR__ . '/../../../../../data/config.ini';

/* Create */
// first insert one row
$row = array ("Name" => "GUEST");
if (\Jobin\Dal::create('Role', $row, 'Name')) {
    echo 'Create is gelukt!<br />';
} else {
    echo 'Oeps er is iets fout gelopen! <br />';
}
echo \Jobin\Dal::getMessage();

// then the other three at one time
$rows = array (array("Name" => "USER"), ["Name" => "SUPERUSER"], ["Name" => "DIRECTOR"]);
echo '<pre>';
var_dump($rows);
echo '</pre>';
if (\Jobin\Dal::create('Role', $rows, 'Name')) {
    echo 'Create is gelukt!<br />';
} else {
    echo 'Oeps er is iets fout gelopen! <br />';
}
echo \Jobin\Dal::getMessage();
/* ReadAll */
$list = \Jobin\Dal::readAll('Role', 'Name');
echo '<br />Alles:<br />';
foreach($list as $item) {
    echo $item['Name'] . ' ' . $item['Id'] . '<br>';
}
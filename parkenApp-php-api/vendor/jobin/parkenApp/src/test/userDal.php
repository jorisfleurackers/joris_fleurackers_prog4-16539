<?php
include ('../../../../autoload.php');

\Jobin\Dal::$configLocation = __DIR__ . '/../../../../../data/config.ini';

/* Create */
$rows = array (
    ["Name" => "Kees", "HashedPassword" => password_hash('Modern_Waysf5Yu6èH,kk', PASSWORD_DEFAULT), 'RoleId' => '1', 'Salt' => 'f5Yu6èH,kk'], 
    ["Name" => "Liesbeth", "HashedPassword" => password_hash('Modern_WaysvB&z@fg,jj', PASSWORD_DEFAULT), 'RoleId' => '2', 'Salt' => 'vB&z@fg,jj']
);
if (\Jobin\Dal::create('User', $rows, 'Name')) {
    echo 'Create is gelukt!<br />';
} else {
    echo 'Oeps er is iets fout gelopen! <br />';
}
echo \Jobin\Dal::getMessage();
/* ReadAll */
$list = \Jobin\Dal::readAll('User', 'Name');
echo '<br />Alles:<br />';
foreach($list as $item) {
    echo $item['Name'] . ' ' . $item['Id'] . '<br />';
}

<?php
namespace Jobin\ParkenApp;

class Setup
{
    /**
     * initialize tables
     */
    public static function fillCuriosityFromJSON()
    {
        $list = json_decode(file_get_contents(__DIR__ . '/../../../../data/parken.json'), true);
        $curiosityList = $list['Curiosity'];

        foreach ($curiosityList as $item) {
            //Remove the "Id" element, which has the index "Id".
            // Om te inserten hebben we geen Id nodig
            unset($item['Id']);
            // print_r($item);
            \Jobin\Dal::create('Curiosity', $item);
            echo \Jobin\Dal::getMessage() . '<br />';
        }
    }
}

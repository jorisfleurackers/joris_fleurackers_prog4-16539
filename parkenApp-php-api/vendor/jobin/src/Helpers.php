<?php
namespace Jobin;

class Helpers {
    public static function escape($html) {
	    return htmlspecialchars($html, ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
    }
    
    public static function cleanUpInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    function isAssoc($var) {
        return is_array($var) && array_diff_key($var, array_keys(array_keys($var)));
    }
}
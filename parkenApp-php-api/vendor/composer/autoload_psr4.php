<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Jobin\\ParkenApp\\' => array($vendorDir . '/jobin/parkenApp/src'),
    'Jobin\\' => array($vendorDir . '/jobin/src'),
);

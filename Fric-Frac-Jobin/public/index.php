<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <title>Beheer Fric-frac</title>
</head>

<body>
    <header>
        <h1>Fric-frac</h1>
    </header>
    <main>
        <section>
            <a href="Person/Index.php" class="tile"><span>Persoon</span></a>
            <a href="Country/Index.php" class="tile"><span>Land</span></a>
            <div class="tile"></div>
            <div class="tile"></div>
            <a href="Role/Index.php" class="tile"><span>Role</span></a>
            <div class="tile"><span>Gebruiker</span></div>
            <div class="tile"></div>
            <a href="Event/Index.php" class="tile"><span>Event</span></a>
            <a href="EventCategory/Index.php" class="tile"><span>Event Categorie</span></a>
            <a href="EventTopic/Index.php" class="tile"><span>Event Topic</span></a>
            <div class="tile"></div>
        </section>
    </main>
    <?php include('./templates/footer.php'); ?>
</body>

</html>
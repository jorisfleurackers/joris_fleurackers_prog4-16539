<?php
    $list = \ModernWays\FricFrac\Dal\Event::readAll();
?>
<aside>
    <table>

        <?php
            if ($list) {
                foreach ($list as $item) {
                    if ($item['Id'] === $id) {
                        ?>
        <tr style="background-color:
                   #666666;">
            <td><a href="ReadingOne.php?Id=<?php echo $item['Id']; ?>"></a></td>
            <?php
                    } else {
                        ?>
        <tr>
            <td><a href="ReadingOne.php?Id=<?php echo $item['Id']; ?>">-></a></td>
            <?php
                    } ?>
            <td><?php echo $item['Name']; ?></td>
            <td><?php echo $item['Starts']; ?></td>
        </tr>
        <?php
                }
            } else {
                ?>
        <tr>
            <td>Geen events gevonden!!</td>
        </tr>
        <?php
            }
        ?>
    </table>
</aside>
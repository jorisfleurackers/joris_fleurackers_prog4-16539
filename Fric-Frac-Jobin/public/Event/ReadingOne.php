<?php
    include('../templates/header.php');
    $categoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
    $topicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
    $id = $_GET['Id'];
    // echo $id;
    $model = new \ModernWays\FricFrac\Model\Event();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Event::readOneById($id));

    
if (isset($_POST['uc'])) {
    switch ($_POST['uc']) {
        case 'delete':
            \ModernWays\FricFrac\Dal\Event::delete($id);
            header("Location: Index.php");
            break;
        case 'update':
            $model = new \ModernWays\FricFrac\Model\Event();
            $model->setName($_POST['Name']);
            $model->setLocation($_POST['Location']);
            $model->setImage($_POST['Image']);
            $model->setStarts($_POST['StartDate'] . ' ' . $_POST['StartTime']);
            $model->setEnds($_POST['EndDate'] . ' ' . $_POST['EndTime']);
            $model->setDescription($_POST['Description']);
            $model->setOrganiserName($_POST['OrganiserName']);
            $model->setOrganiserDescription($_POST['OrganiserDescription']);
            $model->setEventCategoryId($_POST['EventCategoryId']);
            $model->setEventTopicId($_POST['EventTopicId']);
        // var_dump($_POST);
            \ModernWays\FricFrac\Dal\Event::update($model->toArray(), $id);
            header("Location: Index.php");
            break;
        default:
            break;
    }
}
?>



<main>
    <article>
        <header>
            <nav>
                <h2 class="titelNaam">Event</h2>
                <div class="navBar">
                    <a class="button navItem" href="Index.php">Terug</a>
                    <button class="button navItem" type="submit" name="uc" value="delete" form="form">Delete</button>
                    <button class="button navItem" type="submit" name="uc" value="update" form="form">Update</button>
                    <!-- <a class="button navItem" href="InsertingOne.php">Insert</a> -->
                </div>
            </nav>
        </header>
        <div class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td><label for="Name">Naam</label></td>
                        <td><input type="text" required id="Name" name="Name"
                                value="<?php echo $model->getName(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="Location">Plaats</label></td>
                        <td><input type="text" id="Location" name="Location"
                                value="<?php echo $model->getLocation(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="StartTime">Starttijd</label></td>
                        <td><input type="time" id="StartTime" name="StartTime"
                                value="<?php echo $model->getStartTime(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="StartDate">Startdatum</label></td>
                        <td><input type="date" id="StartDate" name="StartDate"
                                value="<?php echo $model->getStartDate(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="EndTime">Einde</label></td>
                        <td><input type="time" id="EndTime" name="EndTime"
                                value="<?php echo $model->getEndTime(); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="EndDate">Einddatum</label></td>
                        <td><input type="date" id="EndDate" name="EndDate"
                                value="<?php echo $model->getEndDate(); ?>" /></td>
                    </tr>
                    <div>
                        <td><label for="Image">Afbeelding</label></td>
                        <td><input type="text" id="Image" name="Image" value="<?php echo $model->getImage(); ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="Description">Beschrijving</label></td>
                            <td><input type="text" required id="Description" name="Description"
                                    value="<?php echo $model->getDescription(); ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="OrganiserName">Naam organisator</label></td>
                            <td><input type="text" required id="OrganiserName" name="OrganiserName"
                                    value="<?php echo $model->getOrganiserName(); ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="OrganiserDescription">Beschrijving organisator</label></td>
                            <td><input type="text" id="OrganiserDescription" name="OrganiserDescription"
                                    value="<?php echo $model->getOrganiserDescription(); ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="EventCategoryId">Event categorie</label></td>
                            <td><select id="EventCategoryId" name="EventCategoryId">
                                    <!-- option elementen -->
                                    <?php
                                    if ($categoryList) {
                                        foreach ($categoryList as $row) {
                                            ?>
                                    <option value="<?php echo $row['Id']; ?>"
                                        <?php echo $model->getEventCategoryId() === $row['Id'] ? 'SELECTED' : ''; ?>>
                                        <?php echo $row['Name']; ?>
                                    </option>
                                    <?php
                                        }
                                    }
            ?>
                                </select></td>
                        </tr>
                        <tr>
                            <td><label for="EventTopicId">Event topic</label></td>
                            <td><select id="EventTopicId" name="EventTopicId">
                                    <!-- option elementen -->
                                    <?php
                                                        if ($topicList) {
                                                            foreach ($topicList as $row) {
                                                                ?>
                                    <option value="<?php echo $row['Id']; ?>"
                                        <?php echo $model->getEventTopicId() === $row['Id'] ? 'SELECTED' : ''; ?>>
                                        <?php echo $row['Name']; ?>
                                    </option>
                                    <?php
                                                            }
                                                        }
            ?>
                                </select></td>
                        </tr>
                </table>
            </form>
        </div>

        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../templates/footer.php');?>
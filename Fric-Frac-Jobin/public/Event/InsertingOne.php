<?php
$id = null;
    include('../templates/header.php');
    $categoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
    $topicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
    if (isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Event();
        $model->setName($_POST['Name']);
        $model->setLocation($_POST['Location']);
        $model->setImage($_POST['Image']);
        $model->setStarts($_POST['StartDate'] . ' ' .$_POST['StartTime']);
        $model->setEnds($_POST['EndDate'] . ' ' .$_POST['EndTime']);
        $model->setDescription($_POST['Description']);
        $model->setOrganiserName($_POST['OrganiserName']);
        $model->setOrganiserDescription($_POST['OrganiserDescription']);
        $model->setEventCategoryId($_POST['EventCategoryId']);
        $model->setEventTopicId($_POST['EventTopicId']);
        // var_dump($_POST);
        \ModernWays\FricFrac\Dal\Event::create($model->toArray());
    }
?>
<main>
    <article>
        <header>
            <nav>
                <h2 class="titelNaam">Event</h2>
                <tr class="navBar">
                    <a class="button navItem" href="Index.php">Annuleren</a>
                    <button class="button navItem" type="submit" name="uc" value="insert" form="form">Insert</button>

                </tr>

            </nav>
        </header>
        <tr class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td><label for="Name">Naam</label></td>
                        <td><input type="text" required id="Name" name="Name" /></td>
                    </tr>
                    <tr>
                        <td><label for="Location">Plaats</label></td>
                        <td><input type="text" id="Location" name="Location" /></td>
                    </tr>
                    <tr>
                        <td><label for="StartTime">Starttijd</label></td>
                        <td><input type="time" id="StartTime" name="StartTime" /></td>
                    </tr>
                    <tr>
                        <td><label for="StartDate">Startdatum</label></td>
                        <td><input type="date" id="StartDate" name="StartDate" /></td>
                    </tr>
                    <tr>
                        <td><label for="EndTime">Einde</label></td>
                        <td><input type="time" id="EndTime" name="EndTime" /></td>
                    </tr>
                    <tr>
                        <td><label for="EndDate">Einddatum</label></td>
                        <td><input type="date" id="EndDate" name="EndDate" /></td>
                    </tr>
                    <tr>
                        <td><label for="Image">Afbeelding</label></td>
                        <td><input type="text" id="Image" name="Image" /></td>
                    </tr>
                    <tr>
                        <td><label for="Description">Beschrijving</label></td>
                        <td><input type="text" required id="Description" name="Description" /></td>
                    </tr>
                    <tr>
                        <td><label for="OrganiserName">Naam organisator</label></td>
                        <td><input type="text" required id="OrganiserName" name="OrganiserName" /></td>
                    </tr>
                    <tr>
                        <td><label for="OrganiserDescription">Beschrijving organisator</label></td>
                        <td><input type="text" id="OrganiserDescription" name="OrganiserDescription" /></td>
                    </tr>
                    <tr>
                        <td><label for="EventCategoryId">Event categorie</label></td>
                        <td><select id="EventCategoryId" name="EventCategoryId">
                                <!-- option elementen -->
                                <?php
                    if ($categoryList) {
                        foreach ($categoryList as $row) {
                            ?>
                                <option value="<?php echo $row['Id']; ?>"><?php echo $row['Name']; ?></option>
                                <?php
                        }
                    }
                    ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><label for="EventTopicId">Event topic</label></td>
                        <td><select id="EventTopicId" name="EventTopicId">
                                <!-- option elementen -->
                                <?php
                    if ($topicList) {
                        foreach ($topicList as $row) {
                            ?>
                                <option value="<?php echo $row['Id']; ?>"><?php echo $row['Name']; ?></option>
                                <?php
                        }
                    }
                    ?>
                            </select></td>
                    </tr>
                </table>
            </form>
        </tr>
        <tr id="feedback"></tr>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../templates/footer.php');?>
<?php
    include('../templates/header.php');
    $categoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
    $topicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
    if (isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Event();
        $model->setName($_POST['Name']);
        $model->setLocation($_POST['Location']);
        // bij insert wordt de id automatisch toegevoegd
        // $model->setId($_POST['Id']);
        $model->setImage($_POST['Image']);
        $model->setStarts($_POST['StartDate'] . ' ' .$_POST['StartTime']);
        $model->setEnds($_POST['EndDate'] . ' ' .$_POST['EndTime']);
        $model->setDescription($_POST['Description']);
        $model->setOrganiserName($_POST['OrganiserName']);
        $model->setOrganiserDescription($_POST['OrganiserDescription']);
        $model->setEventCategoryId($_POST['EventCategoryId']);
        $model->setEventTopicId($_POST['EventTopicId']);
        // var_dump($_POST);
        \ModernWays\FricFrac\Dal\Event::create($model->toArray());
    }
?>
<main>
    <article>
        <header>
            <h2>Event</h2>
            <nav>
                <button type="submit" name="uc" value="insert" form="form">Insert</button>
                <a href="Index.php">Annuleren</a>
            </nav>
        </header>
        <form id="form" action="" method="POST">
            <table class="formTable">
                <tr>
                    <td><label for="Name">Naam</label></td>
                    <td><input type="text" required id="Name" name="Name" />
                </tr>
                <tr>
                    <td><label for="Location">Plaats</label></td>
                    <td><input type="text" id="Location" name="Location" />
                </tr>
                <tr>
                    <td><label for="StartTime">Starttijd</label></td>
                    <td><input type="time" id="StartTime" name="StartTime" />
                </tr>
                <tr>
                    <td><label for="StartDate">Startdatum</label></td>
                    <td><input type="date" id="StartDate" name="StartDate" />
                </tr>
                <tr>
                    <td><label for="EndTime">Einde</label></td>
                    <td><input type="time" id="EndTime" name="EndTime" />
                </tr>
                <tr>
                    <td><label for="EndDate">Einddatum</label></td>
                    <td><input type="date" id="EndDate" name="EndDate" />
                </tr>
                <tr>
                    <td><label for="Image">Afbeelding</label></td>
                    <td><input type="text" id="Image" name="Image" />
                </tr>
                <tr>
                    <td><label for="Description">Beschrijving</label></td>
                    <td><input type="text" required id="Description" name="Description" />
                </tr>
                <tr>
                    <td><label for="OrganiserName">Naam organisator</label></td>
                    <td><input type="text" required id="OrganiserName" name="OrganiserName" />
                </tr>
                <tr>
                    <td><label for="OrganiserDescription">Beschrijving organisator</label></td>
                    <td><input type="text" id="OrganiserDescription" name="OrganiserDescription" />
                </tr>
                <tr>
                    <td><label for="EventCategoryId">Event categorie</label></td>
                    <select id="EventCategoryId" name="EventCategoryId">
                        <!-- option elementen -->
                        <?php
                    if ($categoryList) {
                        foreach ($categoryList as $row) {
                            ?>
                        <option value="<?php echo $row['Id']; ?>"><?php echo $row['Name']; ?></option>
                        <?php
                        }
                    }
                    ?>
                    </select>
                </tr>
                <tr>
                    <td><label for="EventTopicId">Event topic</label></td>
                    <select id="EventTopicId" name="EventTopicId">
                        <!-- option elementen -->
                        <?php
                    if ($topicList) {
                        foreach ($topicList as $row) {
                            ?>
                        <option value="<?php echo $row['Id']; ?>"><?php echo $row['Name']; ?></option>
                        <?php
                        }
                    }
                    ?>
                    </select>
                </tr>
            </table>
        </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../templates/footer.php');?>
<?php
$id = null;
include('../templates/header.php');
$countryList = \ModernWays\FricFrac\Dal\Country::readAll();
if (isset($_POST['uc'])) {
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->setFirstName($_POST['FirstName']);
    $model->setLastName($_POST['LastName']);
    $model->setEmail($_POST['Email']);
    $model->setAddress1($_POST['Address1']);
    $model->setAddress2($_POST['Address2']);
    $model->setPostalCode($_POST['PostalCode']);
    $model->setCity($_POST['City']);
    $model->setCountryId($_POST['CountryId']);
    $model->setPhone1($_POST['Phone1']);
    $model->setBirthday($_POST['Birthday']);
    $model->setRating($_POST['Rating']);
        
    // var_dump($_POST);
    \ModernWays\FricFrac\Dal\Person::create($model->toArray());
}
?>
<main>
    <article>
        <header>


            <nav>
                <h2 class="titelNaam">Persoon</h2>
                <div class="navBar">
                    <a class="button navItem" href="Index.php">Annuleren</a>
                    <button class="button navItem" type="submit" name="uc" value="insert" form="form">Insert</button>

                </div>

            </nav>
        </header>
        <div class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td><label for="FirstName">Voornaam</label></td>
                        <td><input type="text" required="required" id="FirstName" name="FirstName" value="" /></td>
                    </tr>
                    <tr>
                        <td> <label for="LastName">Achternaam</label></td>
                        <td> <input type="text" required="required" id="LastName" name="LastName" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="Email">E-mail</label></td>
                        <td> <input type="text" id="Email" name="Email" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="Address1">Adres 1</label></td>
                        <td> <input type="text" id="Address1" name="Address1" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="Address2">Adres 2</label></td>
                        <td><input type="text" id="Address2" name="Address2" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="PostalCode">Postcode</label></td>
                        <td><input type="text" id="PostalCode" name="PostalCode" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="City">Stad</label></td>
                        <td><input type="text" id="City" name="City" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="CountryId">Land</label></td>
                        <td> <select id="CountryId" name="CountryId">
                                <!-- option elementen -->
                                <?php
                    if ($countryList) {
                        foreach ($countryList as $row) {
                            ?>
                                <option value="<?php echo $row['Id']; ?>"><?php echo $row['Name']; ?></option>
                                <?php
                        }
                    }
            ?>
                            </select> </td>
                    </tr>
                    <tr>
                        <td> <label for="Phone1">Telefoonnummer</label></td>
                        <td><input type="text" id="Phone1" name="Phone1" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="Birthday">Geboortedatum</label></td>
                        <td><input type="date" id="Birthday" name="Birthday" value="" /></td>
                    </tr>
                    <tr>
                        <td> <label for="Rating">Tevreden</label></td>
                        <td><input type="range" id="Rating" name="Rating" value="" /></td>
                    </tr>
                </table>

            </form>
        </div>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('../templates/footer.php'); ?>
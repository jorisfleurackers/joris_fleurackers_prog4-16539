<?php
$person = \ModernWays\FricFrac\Dal\Person::readAll();
?>
<aside>
    <table class="aside">
        <thead>
            <td class="tableField">Select</td>
            <td class="tableField">FirstName</td>
            <td class="tableField">LastName</td>
        </thead>

        <?php
        if ($person) {
            foreach ($person as $personItem) {
                if ($personItem['Id'] === $id) {
                    ?>
        <tr style="background-color:
                   #666666;">
            <td><a href="ReadingOne.php?Id=<?php echo $personItem['Id']; ?>"></a></td>
            <?php
                } else {
                    ?>
        <tr>
            <td><a href="ReadingOne.php?Id=<?php echo $personItem['Id']; ?>">-></a></td>
            <?php
                } ?>
            <td><?php echo $personItem['FirstName']; ?></td>
            <td><?php echo $personItem['LastName']; ?></td>

        </tr>
        <?php
            }
        } else {
            ?>
        <tr>
            <td>Geen personen gevonden</td>
        </tr>
        <?php
        }
    ?>
    </table>
</aside>
<?php
    include('../templates/header.php');
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Person();
$countryList = \ModernWays\FricFrac\Dal\Country::readAll();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Person::readOneById($id));
    // var_dump($_POST);

if (isset($_POST['uc'])) {
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->setFirstName($_POST['FirstName']);
    $model->setLastName($_POST['LastName']);
    $model->setEmail($_POST['Email']);
    $model->setAddress1($_POST['Address1']);
    $model->setAddress2($_POST['Address2']);
    $model->setPostalCode($_POST['PostalCode']);
    $model->setCity($_POST['City']);
    $model->setCountryId($_POST['CountryId']);
    $model->setPhone1($_POST['Phone1']);
    $model->setBirthday($_POST['Birthday']);
    $model->setRating($_POST['Rating']);
        
    // var_dump($_POST);
    \ModernWays\FricFrac\Dal\Person::update($model->toArray());
    header("Location: Index.php");
} ?>
<main>
    <article>
        <header>
            <h2>Persoon</h2>
        <nav>
            <button type="submit" name="uc" value="update" form="form">Update</button>
           <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
           <div>
                <label for="FirstName">Voornaam</label>
                <input type="text"  id="FirstName" name="FirstName"
                    value="<?php echo $model->getFirstName();?>"/>
            </div>
             <div>
                <label for="LastName">Achternaam</label>
                <input type="text"  id="LastName" name="LastName"
                    value="<?php echo $model->getLastName();?>"/>
            </div>
             <div>
                <label for="Email">E-mail</label>
                <input type="text"  id="Email" name="Email"
                    value="<?php echo $model->getEmail();?>"/>
            </div>
             <div>
                <label for="Address1">Adres 1</label>
                <input type="text"  id="Address1" name="Address1"
                    value="<?php echo $model->getAddress1();?>"/>
            </div>
             <div>
                <label for="Address2">Adres 2</label>
                <input type="text"  id="Address2" name="Address2"
                    value="<?php echo $model->getAddress2();?>"/>
            </div>
             <div>
                <label for="PostalCode">Postcode</label>
                <input type="text"  id="PostalCode" name="PostalCode"
                    value="<?php echo $model->getPostalCode();?>"/>
            </div>
             <div>
                <label for="City">Stad</label>
                <input type="text"  id="City" name="City"
                value="<?php echo $model->getCity();?>"/>
            </div>
             <div>
                <label for="CountryId">Land</label>
                <select id="CountryId" name="CountryId">
                    <!-- option elementen -->
                     <?php
                    if ($countryList) {
                        foreach ($countryList as $row) {
                            ?>
                    <option value="<?php echo $row['Id']; ?>"
                        <?php echo $model->getCountryId() === $row['Id'] ? 'SELECTED' : ''; ?>>
                        <?php echo $row['Name']; ?>
                    </option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>
             <div>
                <label for="Phone1">Telefoonnummer</label>
                <input type="text"  id="Phone" name="Phone1"
                    value="<?php echo $model->getPhone1();?>"/>
            </div>
             <div>
                <label for="Birthday">Geboortedatum</label>
                <input type="date"  id="Birthday" name="Birthday"
                    value="<?php echo $model->getBirthday();?>"/>
            </div>
             <div>
                <label for="Rating">Tevreden</label>
                <input type="range"  id="Rating" name="Rating"
                    value="<?php echo $model->getRating();?>"/>
            </div>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../templates/footer.php');?>
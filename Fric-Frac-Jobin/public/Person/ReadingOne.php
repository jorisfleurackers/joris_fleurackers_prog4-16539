<?php
include('../templates/header.php');
$countryList = \ModernWays\FricFrac\Dal\Country::readAll();
$id = $_GET['Id'];
    // echo $id;
$model = new \ModernWays\FricFrac\Model\Person();
$model->arrayToObject(\ModernWays\FricFrac\Dal\Person::readOneById($id));
    // var_dump($_POST);

if (isset($_POST['uc'])) {
    switch ($_POST['uc']) {
        case 'delete':
            \ModernWays\FricFrac\Dal\Person::delete($id);
            header("Location: Index.php");
            break;
        case 'update':
            $model->setFirstName($_POST['FirstName']);
            $model->setLastName($_POST['LastName']);
            $model->setEmail($_POST['Email']);
            $model->setAddress1($_POST['Address1']);
            $model->setAddress2($_POST['Address2']);
            $model->setPostalCode($_POST['PostalCode']);
            $model->setCity($_POST['City']);
            $model->setCountryId($_POST['CountryId']);
            $model->setPhone1($_POST['Phone1']);
            $model->setBirthday($_POST['Birthday']);
            $model->setRating($_POST['Rating']);
            \ModernWays\FricFrac\Dal\Person::update($model->toArray());
            header("Location: Index.php");
            break;
        default:
            break;
    }
}
?>
<main>
    <article>
        <header>
            <nav>
                <h2 class="titelNaam">Persoon</h2>
                <div class="navBar">
                    <a class="button navItem" href="Index.php">Terug</a>
                    <button class="button navItem" type="submit" name="uc" value="delete" form="form">Delete</button>
                    <button class="button navItem" type="submit" name="uc" value="update" form="form">Update</button>
                    <!-- <a class="button navItem" href="InsertingOne.php">Insert</a> -->
                </div>
            </nav>
        </header>
        <div class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td><label for="FirstName">Voornaam</label></td>
                        <td><input type="text" id="FirstName" name="FirstName"
                                value="<?php echo $model->getFirstName(); ?>" /></td>
                    </tr>
                    <tr>
                        <td> <label for="LastName">Achternaam</label></td>
                        <td> <input type="text" id="LastName" name="LastName"
                                value="<?php echo $model->getLastName(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="Email">E-mail</label></td>
                        <td> <input type="text" id="Email" name="Email" value="<?php echo $model->getEmail(); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="Address1">Adres 1</label></td>
                        <td> <input type="text" id="Address1" name="Address1"
                                value="<?php echo $model->getAddress1(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="Address2">Adres 2</label></td>
                        <td><input type="text" id="Address2" name="Address2"
                                value="<?php echo $model->getAddress2(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="PostalCode">Postcode</label></td>
                        <td><input type="text" id="PostalCode" name="PostalCode"
                                value="<?php echo $model->getPostalCode(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="City">Stad</label></td>
                        <td><input type="text" id="City" name="City" value="<?php echo $model->getCity(); ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="CountryId">Land</label></td>
                        <td> <select id="CountryId" name="CountryId">
                                <?php
                    if ($countryList) {
                        foreach ($countryList as $row) {
                            ?>
                                <option value="<?php echo $row['Id']; ?>"
                                    <?php echo $model->getCountryId() === $row['Id'] ? 'SELECTED' : ''; ?>>
                                    <?php echo $row['Name']; ?>
                                </option>
                                <?php
                        }
                    }
            ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td> <label for="Phone1">Telefoonnummer</label></td>
                        <td><input type="text" id="Phone1" name="Phone1" value="<?php echo $model->getPhone1(); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="Birthday">Geboortedatum</label></td>
                        <td><input type="date" id="Birthday" name="Birthday"
                                value="<?php echo $model->getBirthday(); ?>" /></td>
                    </tr>
                    <tr>
                        <td> <label for="Rating">Tevreden</label></td>
                        <td><input type="range" id="Rating" name="Rating" value="<?php echo $model->getRating(); ?>" />
                        </td>
                    </tr>
                </table>

            </form>
        </div>

        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('../templates/footer.php'); ?>
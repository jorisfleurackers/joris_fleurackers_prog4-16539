<?php
    $eventTopic = \ModernWays\FricFrac\Dal\EventTopic::readAll();
?>
<aside>
    <table class="aside">
        <thead>
            <td class="tableField">Select</td>
            <td class="tableField">Name</td>
        </thead>
        <?php
            if ($eventTopic) {
                foreach ($eventTopic as $eventTopicItem) {
                    if ($eventTopicItem['Id'] === $id) {
                        ?>
        <tr style="background-color:
                   #666666;">
            <td><a href="ReadingOne.php?Id=<?php echo $eventTopicItem['Id']; ?>"></a></td>
            <?php
                    } else {
                        ?>
        <tr>
            <td><a href="ReadingOne.php?Id=<?php echo $eventTopicItem['Id']; ?>">-></a></td>
            <?php
                    } ?>
            <td><?php echo $eventTopicItem['Name']; ?></td>

        </tr>
        <?php
                }
            } else {
                ?>
        <tr>
            <td>Geen eventTopic gevonden</td>
        </tr>
        <?php
            }
        ?>
    </table>
</aside>
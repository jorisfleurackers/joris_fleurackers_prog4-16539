<?php
    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();
?>
<aside>
    <table class="aside">
        <thead class=tabH>
            <td class="tableField">Select</td>
            <td class="tableField">Name</td>
            <td class="tableField">Code</td>
        </thead>

        <?php
            if ($countryList) {
                foreach ($countryList as $countryItem) {
                if ($countryItem['Id'] === $id) {
                    ?>
        <tr style="background-color:
                   #666666;">
            <td><a href="ReadingOne.php?Id=<?php echo $countryItem['Id']; ?>"></a></td>
            <?php

        } else {
            ?>
        <tr>
            <td><a href="ReadingOne.php?Id=<?php echo $countryItem['Id']; ?>">-></a></td>
            <?php

        } ?>
            <td><?php echo $countryItem['Name']; ?></td>
            <td><?php echo $countryItem['Code']; ?></td>
        </tr>
        <?php
                }
            } else {
                ?>
        <tr>
            <td>Geen landen gevonden</td>
        </tr>
        <?php
            }
        ?>
    </table>
</aside>
<?php
include('../templates/header.php');
if (isset($_POST['uc'])) {
    $model = new \ModernWays\FricFrac\Model\Country();
    $model->setName($_POST['Name']);
    $model->setCode($_POST['Code']);
    // var_dump($_POST);
    \ModernWays\FricFrac\Dal\Country::create($model->toArray());
}
?>
<main>
    <article>
        <header>
            <h2>Land</h2>

            <nav>
                <h2 class="titelNaam">Land</h2>
                <div class="navBar">
                    <a class="button navItem" href="Index.php">Annuleren</a>
                    <button class="button navItem" type="submit" name="uc" value="insert" form="form">Insert</button>

                </div>

            </nav>
        </header>
        <div class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td> <label for="Name">Naam</label></td>
                        <td><input type="text" required id="Name" name="Name" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="Code">Code</label></td>
                        <td><input type="text" required id="Code" name="Code" value="" /></td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../templates/footer.php');?>
<?php
    $RoleList = \ModernWays\FricFrac\Dal\Role::readAll();
?>
<aside>
    <table class="aside">
        <thead>
            <td class="tableField">Select</td>
            <td class="tableField">Name</td>
        </thead>
        <?php
        if ($RoleList) {
            foreach ($RoleList as $RoleItem) {
                if ($RoleItem['Id'] === $id) {
                    ?>
        <tr style="background-color:
                   #666666;">
            <td><a href="ReadingOne.php?Id=<?php echo $RoleItem['Id']; ?>"></a></td>
            <?php
                } else {
                    ?>
        <tr>
            <td><a href="ReadingOne.php?Id=<?php echo $RoleItem['Id']; ?>">-></a></td>
            <?php
                } ?>


            <td><?php echo $RoleItem['Name']; ?></td>

        </tr>
        <?php
            }
        } else {
            ?>
        <tr>
            <td>Geen roles gevonden</td>
        </tr>
        <?php
        }
        ?>
    </table>
</aside>
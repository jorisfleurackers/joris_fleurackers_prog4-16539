<?php
$id = null;
    include('../templates/header.php');
    if (isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Role();
        $model->setName($_POST['Name']);
        // var_dump($_POST);
        \ModernWays\FricFrac\Dal\Role::create($model->toArray());
    }
?>
<main>
    <article>
        <header>
            <nav>
                <h2 class="titelNaam">Rol</h2>
                <div class="navBar">
                    <a class="button navItem" href="Index.php">Annuleren</a>
                    <button class="button navItem" type="submit" name="uc" value="insert" form="form">Insert</button>

                </div>

            </nav>
        </header>
        <div class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td> <label for="Name">Naam</label></td>
                        <td><input type="text" required id="Name" name="Name" value="" /></td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../templates/footer.php');?>
<?php
    $eventCategory = \ModernWays\FricFrac\Dal\EventCategory::readAll();
?>

<aside>
    <table class="aside" id="table">
        <thead>
            <td class="tableField">Select</td>
            <td class="tableField">Name</td>
        </thead>
        <?php
            if ($eventCategory) {
                foreach ($eventCategory as $eventCategoryItem) {
                    if ($eventCategoryItem['Id']===$id) {
                        ?>
        <tr style="background-color:
                   #666666;">
            <td><a href="ReadingOne.php?Id=<?php echo $eventCategoryItem['Id']; ?>"></a></td>
            <?php
                    } else {
                        ?>
        <tr>
            <td><a href="ReadingOne.php?Id=<?php echo $eventCategoryItem['Id']; ?>">-></a></td>
            <?php
                    } ?>


            <td><?php echo $eventCategoryItem['Name']; ?></td>

        </tr>
        <?php
                }
            } else {
                ?>
        <tr>
            <td>Geen eventCategory gevonden</td>
        </tr>
        <?php
            }
        ?>
    </table>
</aside>
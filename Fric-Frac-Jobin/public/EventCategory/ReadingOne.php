<?php
include('../templates/header.php');
$id = $_GET['Id'];
    // echo $id;
$model = new \ModernWays\FricFrac\Model\EventCategory();
$model->arrayToObject(\ModernWays\FricFrac\Dal\EventCategory::readOneById($id));
    // var_dump($_POST);

if (isset($_POST['uc'])) {
    switch ($_POST['uc']) {
        case 'delete':
            \ModernWays\FricFrac\Dal\EventCategory::delete($id);
            header("Location: Index.php");
            break;
        case 'update':
            $model->setName($_POST['Name']);
            \ModernWays\FricFrac\Dal\EventCategory::update($model->toArray());
            header("Location: Index.php");
            break;
        default:
            break;
    }
}
?>
<main>
    <article>
        <header>
            <nav>
                <h2 class="titelNaam">Event Category</h2>
                <div class="navBar">
                    <a class="button navItem" href="Index.php">Terug</a>
                    <button class="button navItem" type="submit" name="uc" value="delete" form="form">Delete</button>
                    <button class="button navItem" type="submit" name="uc" value="update" form="form">Update</button>
                    <!-- <a class="button navItem" href="InsertingOne.php">Insert</a> -->
                </div>
            </nav>
        </header>
        <div class="navField">
            <form id="form" action="" method="POST">
                <table class="formTable">
                    <tr>
                        <td> <label for="Name">Naam</label></td>
                        <td><input type="text" required id="Name" name="Name"
                                value="<?php echo $model->getName(); ?>" /></td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('../templates/footer.php'); ?>




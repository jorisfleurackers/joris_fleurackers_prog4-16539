<?php
    include_once('../../vendor/autoload.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/app.css" type="text/css" />
    <title>Fric-frac</title>
</head>

<body id="main">
    <header>
        <h1 class="title">Fric-frac</h1>
        <div class="container" id="hamburger" onclick="openNav(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>


    </header>

    <div id="mySidenav" class="sidenav">
        <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav(this)">&times;</a> -->
        <a href="../Index.php">Home</a>
        <a href="../Role/Index.php">Rollen</a>
        <a href="../Country/Index.php">Landen</a>
        <a href="../Person/Index.php">Personen</a>
        <a href="../Event/Index.php">Events</a>
        <a href="../EventCategory/Index.php">Event Categories</a>
        <a href="../EventTopic/Index.php">Event Topics</a>
    </div>


    <script>
    function openNav(x) {
        x.classList.toggle("change");
        document.getElementById("hamburger").onclick = function() {
            closeNav(this)
        };
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("mySidenav").style.border = "solid 2px black";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "#cccccc";
    }

    function closeNav(x) {

        x.classList.toggle("change");
        document.getElementById("hamburger").onclick = function() {
            openNav(this)
        };
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("mySidenav").style.border = "solid 0px black";
        document.getElementById("main").style.marginLeft = "0em";
        document.body.style.backgroundColor = "white";
    }
    </script>
    </div>
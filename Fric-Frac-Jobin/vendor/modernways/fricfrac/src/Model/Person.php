<?php
namespace ModernWays\FricFrac\Model;

class Person
{
    private $firstname;
    private $lastname;
    private $email;
    private $address1;
    private $address2;
    private $postalcode;
    private $city;
    private $countryid;
    private $phone1;
    private $birthday;
    private $rating;
    private $id;
    private $list;
    
    public function setFirstName($value)
    {
        $this->firstname = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setLastName($value)
    {
        $this->lastname = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setEmail($value)
    {
        $this->email = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setAddress1($value)
    {
        $this->address1 = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setAddress2($value)
    {
        $this->address2 = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setPostalCode($value)
    {
        $this->postalcode = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setCity($value)
    {
        $this->city = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setCountryId($value)
    {
        $this->countryid = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setPhone1($value)
    {
        $this->phone1 = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setBirthday($value)
    {
        $this->birthday = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setRating($value)
    {
        $this->rating = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setId($value)
    {
        $this->id = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function getFirstName()
    {
        return $this->firstname;
    }
    public function getLastName()
    {
        return $this->lastname;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getAddress1()
    {
        return $this->address1;
    }
    public function getAddress2()
    {
        return $this->address2;
    }
    public function getPostalCode()
    {
        return $this->postalcode;
    }
    public function getCity()
    {
        return $this->city;
    }
    public function getCountryId()
    {
        return $this->countryid;
    }
    public function getPhone1()
    {
        return $this->phone1;
    }
    public function getBirthday()
    {
        return $this->birthday;
    }
    public function getRating()
    {
        return $this->rating;
    }
    public function getId()
    {
        return $this->id;
    }
    
    public function toArray()
    {
        return array(
            "FirstName" => $this->getFirstName(),
            "LastName" => $this->getLastName(),
            "Email" => $this->getEmail(),
            "Address1" => $this->getAddress1(),
            "Address2" => $this->getAddress2(),
            "PostalCode" => $this->getPostalCode(),
            "City" => $this->getCity(),
            "CountryId" => $this->getCountryId(),
            "Phone1" => $this->getPhone1(),
            "Birthday" => $this->getBirthday(),
            "Rating" => $this->getRating(),
            "Id" => $this->getId());
    }
    
    public function arrayToObject($array)
    {
        $this->setFirstName($array['FirstName']);
        $this->setLastName($array['LastName']);
        $this->setEmail($array['Email']);
        $this->setAddress1($array['Address1']);
        $this->setAddress2($array['Address2']);
        $this->setPostalCode($array['PostalCode']);
        $this->setCity($array['City']);
        $this->setCountryId($array['CountryId']);
        $this->setPhone1($array['Phone1']);
        $this->setBirthday($array['Birthday']);
        $this->setRating($array['Rating']);
        $this->setId($array['Id']);
    }
}

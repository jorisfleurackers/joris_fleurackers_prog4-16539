
DROP TABLE IF EXISTS Person;


CREATE TABLE Person
(
    Id INT(11)
    UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	FirstName NVARCHAR
    (50) NOT NULL,
	LastName NVARCHAR
    (120) NOT NULL,
	Email NVARCHAR
    (255),
	Address1 NVARCHAR
    (255),
	Address2 NVARCHAR
    (255),
	PostalCode NVARCHAR
    (20),
	City NVARCHAR
    (80),
	CountryId INT
    (11) UNSIGNED NOT NULL,
	Phone1 NVARCHAR
    (25),
	Birthday DATE,
	Rating INT
    (11)

);

    ALTER TABLE Person
ADD FOREIGN KEY (CountryId) REFERENCES Country(Id);

CREATE DATABASE Tania;

use Tania;

CREATE TABLE users
(
    id INT(11)
    UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	firstname VARCHAR
    (30) NOT NULL,
	lastname VARCHAR
    (30) NOT NULL,
	location VARCHAR
    (50)
);
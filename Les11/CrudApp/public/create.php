<?php
if (isset($_POST["submit"])) {
    include "../config.php";
    include "../common.php";

    try {
        $connection = new \PDO($host, $username, $password, $options);
        $new_user = array("Firstname" => $_POST["FirstName"], "LastName" => $_POST["LastName"], "Location" => $_POST["Location"]);
        
        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "users",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
    );

        $statement = $connection->prepare($sql);
        $statement->execute($new_user);
    } catch (\PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}

?>

<?php include "templates/header.php"; ?>
<?php if (isset($_POST['submit']) && $statement) {
    ?>
	<blockquote><?php echo escape($_POST['FirstName'] . " " . escape($_POST['LastName'])); ?> is succesvol toegevoegd.</blockquote>
<?php
} ?>
<h2>Add a user</h2>
<form action="" method="post">
<div>
    <label for="FirstName">Voornaam</label>
    <input type="text" name="FirstName" id="FirstName">
    </div>
    <div>
    <label for="LastName">Achternaam</label>
    <input type="text" name="LastName" id="LastName">
    </div>
    <div>
    <label for="Location">Plaats</label>
    <input type="text" name="Location" id="Location">
    </div>
    <br>
<button type="submit" name="submit">Verzenden</button>
</form>  
<br>
<a href="index.php">back to home</a>
<?php include "templates/footer.php"; ?>
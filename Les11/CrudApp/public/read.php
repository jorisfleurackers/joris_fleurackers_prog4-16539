<?php

/**
 * Function to query information based on
 * a parameter: in this case, location.
 *
 */

if (isset($_POST['submit'])) {
    try {
        require "../config.php";
        require "../common.php";

        $connection = new \PDO($host, $username, $password, $options);

        $sql = "SELECT * 
						FROM users
						WHERE location = :Location";

        $location = $_POST['Location'];

        $statement = $connection->prepare($sql);
        $statement->bindParam(':Location', $location, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();
    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>
<?php require "templates/header.php"; ?>
		
<?php
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) {
        ?>
		<h2>Results</h2>

		<table>
			<thead>
				<tr>					
					<th>First Name</th>
					<th>Last Name</th>
					<th>Location</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach ($result as $row) {
            ?>
			<tr>
				<td><?php echo escape($row["firstname"]); ?></td>
				<td><?php echo escape($row["lastname"]); ?></td>			
				<td><?php echo escape($row["location"]); ?></td>
			</tr>
		<?php
        } ?> 
			</tbody>
	</table>
	<?php
    } else {
        ?>
		<blockquote>No results found for <?php echo escape($_POST['Location']); ?>.</blockquote>
	<?php
    }
} ?> 

<h2>Find user based on location</h2>

<form method="post">
	<label for="Location">Location</label>
	<input type="text" id="Location" name="Location">
	<input type="submit" name="submit" value="View Results">
</form>

<a href="index.php">Back to home</a>

<?php require "templates/footer.php"; ?>
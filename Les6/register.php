<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="form.css">
    <title>Document</title>
  </head>
  <body>

  <?php
// define variables and set to empty values
$firstname = $lastname= $email = $password1 = $password2 = $address1 = $address2 = $city = $postalcode = $country = $birthday = $satisfied =$sex =$modules =$submit =$gsm =$error ="";
$firstnameErr = $lastnameErr = $emailErr = $password1Err = $password2Err = $address1Err = $cityErr = $postalcodeErr = $countryErr = "";




if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["firstname"])) {
        $firstnameErr = " Voornaam is verplicht";
        $error = true;
    } else {
        $firstname = test_input($_POST["firstname"]);
    }
    if (empty($_POST["lastname"])) {
        $lastnameErr = " Achternaam is verplicht";
        $error = true;
    } else {
        $lastname = test_input($_POST["lastname"]);
    }
    if (empty($_POST["email"])) {
        $emailErr = " Emailadres is verplicht";
        $error = true;
    } else {
        $email = test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = " Verkeerd formaat";
        }
    }
    if (empty($_POST["password1"])) {
        $password1Err = "Wachtwoord is verplicht";
        $error = true;
    } else {
        $password1 = test_input($_POST("password1"));
    }
    if (empty($_POST["password2"])) {
        $password2Err = "Bevestig Wachtwoord is verplicht";
        $error = true;
    } else {
        $password2 = test_input($_POST("password2"));
    }
    if (empty($_POST["address1"])) {
        $address1Err = " Adres is verplicht";
        $error = true;
    } else {
        $address1 = test_input($_POST["address1"]);
    }
    if (empty($_POST["city"])) {
        $cityErr = " Stad is verplicht";
        $error = true;
    } else {
        $city = test_input($_POST["city"]);
    }
    if (empty($_POST["country"])) {
        $countryErr = " Land is verplicht";
        $error = true;
    } else {
        $country = test_input($_POST["country"]);
    }
    if ($error === false) {
        header("Location: newpage.php");
    }
}






function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<span>Vul het registratieformulier in. Velden met een * zijn verplicht</span>
<br><br>
<div class ="formulier">
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
      <fieldset>
        <legend>Accountgegevens</legend>
        <div>
          <label for="firstname">Voornaam <span>*</span></label>
          <input
            type="text"
            name="firstname"
            id="firstname"
            
            value="<?php echo $firstname;?>"
          />
          <span><?php echo "$firstnameErr"?></span>
        </div>
        <div>
          <label for="lastname">Familienaam <span>*</span></label>
          <input
            type="text"
            name="lastname"
            id="lastname"
            value="<?php echo $lastname;?>"
            
          />
          <span><?php echo "$lastnameErr"?></span>
        </div>
        <div>
          <label for="email">E-mail <span> </span></label>
          <input
            type="email"
            name="email"
            id="email"
            
            placeholder="name@example.com"
            value="<?php echo $email;?>"
          />
          <span><?php echo "$emailErr"?></span>
        </div>
        <div>
          <label for="password1">Wachtwoord <span>*</span></label>
          <input
            type="password"
            name="password1"
            id="password1"
            
            value="<?php echo $password1;?>"
          />
          <span><?php echo "$password1Err"?></span>
        </div>
        <div>
          <label for="password2">Wachtwoord bevestigen <span>*</span></label>
          <input
            type="password"
            name="password2"
            id="password2"
            
            value="<?php echo $password2;?>"
          />
          <span><?php echo "$password2Err"?></span>
        </div>
      </fieldset>
      <fieldset>
        <legend>Adresgegevens</legend>
        <div>
          <label for="address1">Adres 1 <span>*</span></label>
          <input
            type="text"
            name="address1"
            id="address1"
            
            value="<?php echo $address1;?>"
          />
          <span><?php echo "$address1Err"?></span>
        </div>
        <div>
          <label for="address2">Adres 2</label>
          <input type="text" name="address2" id="address2" value="<?php echo $address2;?>" />
        </div>
        <div>
          <label for="city">Stad <span>*</span></label>
          <input type="text" name="city" id="city"  value="<?php echo $city;?>" />
          <span><?php echo "$cityErr"?></span>
        </div>
        <div>
          <label for="postalcode">Postcode <span>*</span></label>
          <input
            type="text"
            name="postalcode"
            id="postalcode"
            
            value="<?php echo $postalcode;?>"
          />
          <span><?php echo "$postalcodeErr"?></span>
        </div>
        <div>
          <label for="countryCode">Kies een land <span>*</span></label>
          <select name="countryCode" id="countryCode">
            <option selected="selected" value="1">Belgie</option>
            <option value="2">Nederland</option>
          </select>
        </div>
      </fieldset>
      <fieldset>
        <legend>Persoonlijke gegevens</legend>
        <div>
          <label for="gsm">GSM</label> <input type="text" name="gsm" id="gsm" value="<?php echo $gsm;?>"/>
        </div>
        <div>
          <label for="birthday">Geboortedatum</label>
          <input type="date" name="birthday" id="birthday" value="<?php echo $birthday;?>"/>
        </div>
        <div>
          <label for="satisfied">Hoe tevreden ben je? </label
          ><input type="range" min="1" max="5" />
        </div>
        <div>
          <input type="radio" name="sex" <?php if (isset($sex) && $sex=="male") {
    echo "checked";
}?> id="male" value="male" /><label
            for="male"
            >Man</label
          >
          <input type="radio" name="sex" <?php if (isset($sex) && $sex=="female") {
    echo "checked";
}?> id="female" value="female" /><label
            for="female"
            >Vrouw</label
          >
          <input type="radio" name="sex" <?php if (isset($sex) && $sex=="other") {
    echo "checked";
}?> id="unknown" value="unknown" /><label
            for="unknown"
            >Onbekend</label
          >
        </div>
        <div>
          <label for="faculty">Kies een afdeling </label>
          <select name="faculty" id="faculty">
            <option selected="selected" value="1">HBO informatica</option>
            <option value="2">HBO Boekhouden</option></select
          >
        </div>
        <div>
          <label for="course">Kies een module </label>
          <select name="course" id="course">
            <option selected="selected" value="1">Programmeren 4</option>
            <option value="2">Programmeren 5</option></select
          >
        </div>
      </fieldset>
      <input type="submit" value="verzenden" name="action" id="send" />
    </form>
</div>

<?php
echo "<h2>Your Input:</h2>";
echo $firstname;
echo "<br>";
echo $email;
echo "<br>";
echo $lastname;
echo "<br>";
echo $address1;
echo "<br>";
echo $country;
?>
    
  </body>
</html>

<?php
    if (isset($_COOKIE['PHPSESSID'])) {
        $sessionId = $_COOKIE['PHPSESSID'];
        session_id($sessionId);
    }
    session_start([
    'cookie_lifetime' => 300,
]);
    $_SESSION['Hello'] = 'World!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Werken met sessies</title>
</head>
<body>
    <pre>
        <?php print_r($_SESSION);?>
        <?php print_r($_COOKIE);?>
    </pre>
</body>
</html>
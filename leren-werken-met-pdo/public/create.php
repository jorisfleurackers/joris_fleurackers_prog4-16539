<?php
    include('template/header.php');
    // alleen uit te voeren als er op de submit knop is gedrukt
    if (isset($_POST['submit'])) {
        include('../config.php');
        include('../common.php');
        $newUser = array(
            'FirstName' => escape($_POST['FirstName']),
            'LastName' => escape($_POST['LastName']),
            'Email' => escape($_POST['Email']),
            'Age' => escape($_POST['Age']),
            'Location' => escape($_POST['Location'])
        );
        $statement = false;
        try {
            $sql = 'INSERT INTO Users (FirstName, LastName, Email, Age, Location) VALUES (:FirstName, :LastName, :Email, :Age, :Location)';
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            //$statement->bindParam(':FirstName', $newUser['FirstName']);
            //$statement->bindParam(':LastName', $newUser['LastName']);
            //$statement->bindParam(':Email', $newUser['Email']);
            //$statement->bindParam(':Age', $newUser['Age']);
            //$statement->bindParam(':Location', $newUser['Location']);
            //$statement->execute();
             $statement->execute($newUser);
          
        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
    }
?>

<div id="feedback">
<?php
if (isset($_POST['submit']) && $statement) {
    echo "{$newUser['FirstName']} {$newUser['LastName']} is toegevoegd.";
}
?>
</div>
<!-- form>(div>label+input[id][name])*5  -->
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
    <div><label for="FirstName">Voornaam</label><input type="text" id="FirstName" name="FirstName"></div>
    <div><label for="LastName">Familienaam</label><input type="text" id="LastName" name="LastName"></div>
    <div><label for="Email">E-mail</label><input type="email" id="Email" name="Email"></div>
    <div><label for="Age">Leeftijd</label><input type="text" id="Age" name="Age"></div>
    <div><label for="Location">Plaats</label><input type="text" id="Location" name="Location"></div>
    <button type="submit" name="submit" value="create-person">Verzenden</button>
</form>
<?php
    include('template/footer.php');
?>
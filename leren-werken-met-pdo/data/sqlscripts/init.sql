CREATE DATABASE IF NOT EXISTS TaniaRascia;

use TaniaRascia;

DROP TABLE IF EXISTS Users;

CREATE TABLE Users (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	FirstName NVARCHAR(30) NOT NULL,
	LastName NVARCHAR(30) NOT NULL,
	Email NVARCHAR(50) NOT NULL,
	age INT(3),
	Location NVARCHAR(50),
	-- om keywords te escapen, insluiten tussen backticks
	`Date` TIMESTAMP
);
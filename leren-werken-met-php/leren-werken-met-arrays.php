<?php

//assigning
$movies[0] = "The Matrix";
$movies[1] = "The Matrix2";
$movies[2] = "The Matrix3";

$series = array(
    0 => "Westworld",
    1 => "Game of Thrones");
 


//calling
echo "Mijn film is $movies[0] <br>";
echo "Mijn film is $films[1] <br>";

$adres = array(
    'voornaam' => "Joris",
    'familienaam' => "Fleurackers",
);

echo "Mijn naam is {$adres['voornaam']} <br>";


//loops
foreach ($adres as $element) {
    echo "Mijn element is $element <br>";
}

foreach ($adres as $key => $value) {
    echo "Mijn element is $key en de waarde is $value <br>";
}

$adreskopie = $adres;
if ($adreskopie === $adres) {
    echo "is identiek";
} else {
    echo "niet identiek";
}



//array_chunk voorbeeld
echo "<br>";
$arraytest = array(1,2,3,4,5,6);
$arrayChunk = array_chunk($arraytest, 2, true);
print nl2br(print_r($arrayChunk, true));

//array_unshift voorbeeld
echo "<br>";
$arrayUn = array(1,2,3,4,5,6);
array_unshift($arrayUn, 99, 50);
print nl2br(print_r($arrayUn, true));
//array_fill voorbeeld
echo "<br>";
$arrayFilled = array_fill(0, 5, random_int(0, 50));

print nl2br(print_r($arrayFilled, true));

//array_map voorbeeld
echo "<br><br>";
$array1 = array(1,2,3,4,5);
$array2 = array(6,7,8);

function test($x, $y)
{
    return $x + $y;
}

$myarray = array_map("test", $array1, $array2);
var_dump($myarray);

<?php 
    
    bizarrerieMetRekenkundigeOperatoren();
    
    function lerenWerkenMetEcho()
    {
        echo '<h1>leren werken met php</h1><br>';
        echo '<h2>eerste les</h2><br>';
        $voornaam = 'Joris';
        $familienaam = 'Fleurackers';
        echo "Mijn volledige naam is: $voornaam $familienaam";
        // nu met html en css
        echo "<p>Mijn volledige naam is: <span style=\"color: red\"> $voornaam $familienaam</p>";
    }
    
    function verschilTussenByValueEnByReference()
    {
        $voornaam = 'Robin';
        $familienaam = 'Thoen';
        $adresVoornaam = &$voornaam;
        echo "De voornaam is $voornaam<br>";
        $adresVoornaam = 'Joris';
        echo "De voornaam is $voornaam";
    }
    
    function lerenWerkenMetConstanten()
        {
            define('PI',3.14);
            define('MAX',20);
            define('MIN',2);
            echo 'De waarde van pi is ' . PI . '<br>';
            $value = 100;
            if ( $value >= constant('MIN') && $value <= constant('MAX')) {
                echo 'bingo!!!';
            }
            else {
                echo 'Het getal ligt niet tussen de minimum en maximum waarde';
            }
        }
        
        function bizarrerieMetRekenkundigeOperatoren()
        {
            $getal = 1;
            echo $getal++ . '<br>';
            echo $getal;
        }
   
    
?>